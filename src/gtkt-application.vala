/* gtkt-application.vala
 *
 * Copyright (C) 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public errordomain Gtkt.ApplicationError {
    INVALID_WINDOW_ERROR
}

public delegate int Gtkt.TestFunc (Gtk.Widget widget);

public enum Gtkt.Status {
    RUNNING,
    WAITTING,
    PAUSE,
    SUCCESS,
    FAIL
}
/**
 * Application to be used to test custom
 * widgets.
 *
 * Use {@link initialize_widget} to call {@link set_widget}
 * in order to set the custome widget under test and add
 * tests cases using {@link add_test}
 *
 * Each test added calls {@link initialize} signal when the
 * button play is clicked, so you can setup any thing before
 * so you can see if the widget behaives correctly, then {@link check}
 * signal is triggered so you can use it to make assertions
 * on widget contents or status you expect in the given test
 * conditions.
 *
 * At {@link initialize} you should check the current test number
 * so you can setup the current one.
 *
 * Use {@link finished} quit the application
 * to test your widget.
 *
 * Set {@link waiting_for_event} to true before run the application,
 * so you can interact with your widget like clicking and changing
 * values. {@link waiting_for_event} is set to false by default
 * so the application will be closed automatically when {@link timeout}
 * is reached (in seconds)
 *
 * Following code shows you how setup a {@link GLib.Test}, initialize
 * Gtk, set a simple label widget under test and set {@link waiting_for_event}
 * to true so you can see your application.
 *
 * {{{
 *  class GSvgTest.Suite : Object
 *  {
 *      static int main (string[] args)
 *      {
 *          GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
 *          Test.init (ref args);
 *          Test.add_func ("/gtktester/label",
 *          ()=>{
 *              Gtk.init ();
 *              var w = new Gtk.Label ("THIS IS A LABEL UNDER TEST");
 *              var app = new Gtkt.Application ();
 *              app.waiting_for_event = true;
 *              app.initialize_widget.connect (()=>{
 *                   app.add_test ("Test Simple Label Widget", "Demo test for a known Widget");
 *                   app.set_widget (w);
 *              });
 *
 *              app.initialize.connect (()=>{
 *                  message ("Initializing the test");
 *              });
 *
 *              app.finished.connect (()=>{
 *                  message ("Finishing tests");
 *                  app.quit ();
 *              });
 *
 *              app.run ();
 *          });
 *          return Test.run ();
 *      }
 *  }
 * }}}
 */
public class Gtkt.Application : Gtk.Application  {
    private Gtkt.Window window;
    private Gtk.Widget _widget;
    private int _nfails = 0;
    private int _current_ntest = 0;
    internal class Test : Object {
      public string name { get; set; }
      public string description { get; set; }
    }
    private GLib.Queue<Test> _tests = new Queue<Test> ();

    /**
     * Number of current fails
     */
    public int fails { get { return _nfails; } set { _nfails = value; } }
    /**
     * Time, in seconds, before autoclose window.
     *
     * Autoclose hapends only if {@link waiting_for_event} is
     * set to false.
     */
    public int timeout { get; set; default = 1; }
    /**
     * Number of tests
     */
    public int ntests { get { return (int) _tests.length; } }
    /**
     * Current test number
     */
    public int current_ntest { get { return _current_ntest; } }
    /**
     * Disable autoclose to wait for events.
     *
     * If is set to false (default), the application run automatically
     * the first test and waits the time set at {@link timeout} before
     * autoclose the test window.
     *
     * If set to true, the user can interact with the window and
     * the widget under test, until the user closes the window to finish
     * the test. Take care about the build system default timeout, in meson
     * you can interact by 30 seconds before the test fail.
     */
    public bool waiting_for_event { get; set; default = false; }
    /**
     * Event to initialize the widget under test.
     *
     * This signal is rised when {@link run_tests} is called and
     * is the time to set the widget to test.
     *
     * Is the time also, to use {@link add_test}
     */
    public signal void initialize_widget ();

    /**
     * Event to initialize a test.
     *
     * This signal is rised when {@link next_test} is called
     * and you can use {@link current_ntest} to initialize
     * current test.
     */
    public signal void initialize ();
    /**
     * Event to signal test are starte to run
     */
    public signal void running ();
    /**
     * Signal for all tests finished
     */
    public signal void finished ();
    /**
     * Signal to check if actual conditions in widget met assertions
     */
    public signal void check ();

    public Application () {
        GLib.Object (application_id: "org.gsvg.GtkTester");
        GLib.SimpleAction anext = new SimpleAction ("next", null);
		anext.activate.connect (() => {
			this.hold ();
			next_test ();
			this.release ();
		});

		this.add_action (anext);
    }

    protected override void activate () {
        window = new Gtkt.Window (this);
        window.show ();
        initialize_widget ();

        GLib.Timeout.add (1000, ()=>{
            if (waiting_for_event) {
                return true;
            }

            if (--timeout > 0) {
                return true;
            }

            quit ();
            return false;
        });

        next_test ();
    }

    /**
     * Set widget under test. Set it when
     * {@link initialize} signal is handled.
     */
    public void set_widget (Gtk.Widget w) {
        if (window != null) {
            window.set_widget (w);
        }
    }

    /**
     * Updates information about current test.
     *
     * Is triggered when {@link next_test} is called or
     * when the user click in the next button.
     */
    internal void update () {
        if ((_current_ntest - 1) >= _tests.length) {
            return;
        }

        if (_current_ntest == 0) {
            window.set_test_name ("NO CURRENT TEST");
            window.set_description ("");
        } else {
            window.set_test_name (_tests.peek_nth (_current_ntest - 1).name);
            window.set_description (_tests.peek_nth (_current_ntest - 1).description);
        }
    }

    /**
     * Switch to next test.
     *
     * This is called automatically when the user
     * clicks on next test button.
     *
     * This method trigger {@link update} signal to update
     * current test's information in the header bar, then
     * trigger {@link initialize} in order to allow the
     * to setup new conditions for the next test.
     *
     * If no more tests left, {@link finished} is triggered.
     */
    public void next_test () {
        _current_ntest++;
        if (_current_ntest > _tests.length) {
            finished ();
            return;
        }

        update ();
        initialize ();
        check ();
    }

    /**
     * Add a new test with a name and a description.
     *
     * A name and a description is used to setup a new
     * test case; this information will be shown on the
     * main window's headerbar, when {@link next_test}
     * is called.
     */
    public void add_test (string name, string desc) {
        var t = new Test ();
        t.name = name;
        t.description = desc;
        _tests.push_tail (t);
    }

    /**
     * Start running tests
     */
    public void run_tests () throws GLib.Error {
        if (waiting_for_event) {
            window.set_status (Gtkt.Status.WAITTING);
        } else {
            window.set_status (Gtkt.Status.RUNNING);
        }

        if (_nfails == 0) {
            window.set_status (Gtkt.Status.SUCCESS);
        } else {
            window.set_status (Gtkt.Status.FAIL);
        }

        window.set_nfails (_nfails);
        next_test ();

        running ();
    }
    /**
     * Use this method if you want to set a custom
     * CSS file for your widget to use.
     */
    public void set_css_file (GLib.File css)
        requires (css.query_exists ())
        requires (_widget != null)
    {
        try {
            var istream = css.read (null);
            var ostream = new MemoryOutputStream.resizable ();
            ostream.splice (istream,
                              OutputStreamSpliceFlags.NONE,
                              null);
            uint8[] data = ostream.steal_data ();
            data.length = (int) ostream.get_data_size ();
            MemoryInputStream @is = new MemoryInputStream.from_data (data, GLib.free);
		    DataInputStream distream = new DataInputStream (@is);
		    string str = distream.read_upto ("\0", -1, null, null);
		    string[] c = new string[1];
		    c[0] = str;
            _widget.set_css_classes (c);
            istream.close(null);
            ostream.close(null);
        } catch (GLib.Error e) {
            warning ("Error reading CSS file: %s", e.message);
        }
    }
    /**
     * Executes the given function over the widget to test it.
     *
     * Take care to initialize your test in given {@link TestFunc}.
     *
     * Return a value > 0 to increase the number of fails.
     *
     * @param name the short name of your test
     * @param dsc a description for your test
     * @param func a function to call to run your test
     */
    public void execute_test (string name, string dsc, TestFunc func) {
        window.set_test_name (name);
        window.set_description (dsc);
        if (func (_widget) > 0) {
            _nfails++;
        }

        window.set_nfails (_nfails);
    }


    private class ErrorMessage : Gtk.Grid {
        private Gtk.Label ltcase;
        private Gtk.Label lmessage;
        public string test_case { get { return ltcase.label; } }
        public string message { get { return lmessage.label; } }
        construct {
            lmessage = new Gtk.Label ("No message");
            ltcase = new Gtk.Label ("/case");
            attach (lmessage, 0,0,1,1);
            attach (ltcase,0,1,1,1);
        }
        public ErrorMessage (string tcase, string msg) {
            ltcase.label = tcase;
            lmessage.label = tcase +": "+ msg;
        }
    }
}
