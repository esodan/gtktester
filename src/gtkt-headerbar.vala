/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gtkt-headerbar.vala
 *
 * Copyright (C) 2018 Daniel Espinosa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

/**
 *
 */
internal class Gtkt.HeaderBar : Gtk.Box {
  private Gtk.HeaderBar bar;
  Status _status = Status.PAUSE;
  private Gtk.Image image;
  private Gtk.Label lclosing;
  private Gtk.Button bfails;
  private Gtk.Button bnexttest;
  private Gtk.Label _title;

  /**
   * Header bar title
   */
  public string title {
      get {
          return _title.label;
      }
      set {
          _title.label = value;
      }
  }
  /**
   * Message to be displayed for test suite.
   */
  public string message {
    get {
      return lclosing.label;
    }
    set {
      lclosing.label = value;
    }
  }
  /**
   * Change {@link Status} of the test suite.
   */
  public Status status {
    get {
      return _status;
    }
    set {
      _status = value;
      switch (_status) {
        case Status.RUNNING:
          image.icon_name = "gtk-media-play";
          lclosing.label = "Closing automatically...";
        break;
        case Status.WAITTING:
          image.icon_name = "gtk-media-pause";
          lclosing.label = "Waiting for events...";
        break;
        case Status.SUCCESS:
          image.icon_name = "gtk-apply";
          lclosing.label = "All tests passed";
        break;
        case Status.FAIL:
          image.icon_name = "gtk-dialog-warning";
          lclosing.label = "One or more tests fail";
        break;
        case Status.PAUSE:
          image.icon_name = "gtk-dialog-warning";
          lclosing.label = "Tests paused";
        break;
      }
    }
  }

  public int fails_number {
    get { return int.parse (bfails.label); }
    set { bfails.label = value.to_string (); }
  }

  public signal void next ();

  construct {
    bar = new Gtk.HeaderBar ();
    append (bar);
    var bv = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 5);
    var bht = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 5);
    var bhf = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 5);
    bv.append (bht);
    bv.append (bhf);
    image = new Gtk.Image.from_icon_name ("gtk-media-play");
    lclosing = new Gtk.Label ("Closing");
    bfails = new Gtk.Button.with_label ("0");
    bht.append (bfails);
    bht.append (lclosing);

    bnexttest = new Gtk.Button ();
    bnexttest.set_child (image);
    _title = new Gtk.Label ("NO CURRENT TEST");
    bhf.append (bnexttest);
    bhf.append (_title);

    bar.title_widget = bv;

    bnexttest.clicked.connect (()=>{
      next ();
    });
  }

  public HeaderBar () {
      Object (orientation: Gtk.Orientation.VERTICAL, spacing: 0);
  }
}
